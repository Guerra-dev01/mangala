-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 15, 2023 at 08:55 AM
-- Server version: 10.4.24-MariaDB-cll-lve
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mangala_mangala`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logger`
--

CREATE TABLE `logger` (
  `id` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_05_23_153143_create_categoriadenucias_table', 1),
(6, '2023_05_23_153218_create_denucias_table', 1),
(7, '2023_05_23_153242_create_logins_table', 1),
(8, '2023_06_28_064415_create_usuarios_table', 1),
(9, '2023_06_28_065752_create_provincias_table', 1),
(10, '2023_06_28_065853_create_distritos_table', 1),
(11, '2023_06_28_070404_create_tipo_denucias_table', 1),
(12, '2023_06_28_070613_create_tblanexos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'auth_token', 'bc3a0c213ebcd697a1620938f523b3cf78d62e1c5d40f93ba908d8e9ec4a872e', '[\"*\"]', NULL, '2023-07-30 21:07:21', '2023-07-30 21:07:21'),
(2, 'App\\Models\\User', 1, 'auth_token', '2d9696d3feb050961edaa0f1c689ff70d8bde47591a6b914e7c5f2626a448cbc', '[\"*\"]', NULL, '2023-07-30 21:07:38', '2023-07-30 21:07:38'),
(3, 'App\\Models\\User', 1, 'auth_token', '762373d9ffcad401a024cc6087b0b19ad3aaa4ddf356b0fb384ecd948706ebb9', '[\"*\"]', '2023-07-30 21:56:23', '2023-07-30 21:29:17', '2023-07-30 21:56:23'),
(4, 'App\\Models\\User', 1, 'auth_token', '0996fc4c713bf7002c1a0a20b99372741114482aab1fbe2e9b3cdfcb605d3cac', '[\"*\"]', NULL, '2023-07-30 22:28:07', '2023-07-30 22:28:07'),
(5, 'App\\Models\\model_login\\model_login', 1, 'auth_token', '53bda1c56b7da13307c6ec38a76078bc99ae10dffd83ba5877a8c036118baff6', '[\"*\"]', NULL, '2023-07-30 22:32:09', '2023-07-30 22:32:09'),
(6, 'App\\Models\\model_login\\model_login', 1, 'auth_token', '0282dc9a1abd41f35d1b4b1fb96beaba18991ad54211f958f04ac8202af3537c', '[\"*\"]', '2023-08-01 07:39:02', '2023-07-30 06:42:01', '2023-08-01 07:39:02'),
(7, 'App\\Models\\model_login\\model_login', 5, 'auth_token', 'b13f0fce74fc1e12ab3d5e3ff58d48fced79d7197ca093417dd9e5400805cecf', '[\"*\"]', NULL, '2023-07-30 07:33:37', '2023-07-30 07:33:37'),
(8, 'App\\Models\\model_login\\model_login', 6, 'auth_token', '0e84dbb4ad81fccb53b34a261b47a90143f270d9780f005c2baf6e3ed3900a3e', '[\"*\"]', NULL, '2023-07-30 07:35:05', '2023-07-30 07:35:05'),
(9, 'App\\Models\\model_login\\model_login', 7, 'auth_token', '3abd40af597fa2a76b363bf86711e9fcffb78fd8d3a5a270e129cfeba8c2499d', '[\"*\"]', NULL, '2023-07-30 07:44:51', '2023-07-30 07:44:51'),
(10, 'App\\Models\\model_login\\model_login', 8, 'auth_token', '60e59967af245bf79dffb9184c0899d22acf830bf6212165087a2097243cb51a', '[\"*\"]', NULL, '2023-07-30 07:49:12', '2023-07-30 07:49:12'),
(11, 'App\\Models\\model_login\\model_login', 13, 'auth_token', 'ba3d1fc73fddba22bc15378dad296c3b52c7553a764f0b9c44e4ef33a7bf1acc', '[\"*\"]', NULL, '2023-07-30 08:16:55', '2023-07-30 08:16:55'),
(12, 'App\\Models\\model_login\\model_login', 14, 'auth_token', 'da3c2a0603386dcbbac338b388fe1d23b0e2327830737ab23d7bfd512e1cb5d6', '[\"*\"]', NULL, '2023-07-30 08:19:55', '2023-07-30 08:19:55'),
(13, 'App\\Models\\model_login\\model_login', 15, 'auth_token', '3f438d429d175a551a17ad8972a0436c2b5fb10a108d557dc8478f9bbeab13a3', '[\"*\"]', NULL, '2023-07-30 09:12:51', '2023-07-30 09:12:51'),
(14, 'App\\Models\\model_login\\model_login', 16, 'auth_token', 'f6d8a5cdfd821630b04100d8ac923dee91388524d33601fb4bfe38a505941c2a', '[\"*\"]', NULL, '2023-07-30 09:14:19', '2023-07-30 09:14:19'),
(15, 'App\\Models\\model_login\\model_login', 17, 'auth_token', '1c7c04ef6544d42b4b55bcbe1a6aad04a35b74faf7ae333ef390a5cbb1848491', '[\"*\"]', NULL, '2023-07-30 09:18:48', '2023-07-30 09:18:48'),
(16, 'App\\Models\\model_login\\model_login', 18, 'auth_token', '47eaf4bec360ef712a484472a32b362ed7f9d64d25a28c65069f44386ad22b70', '[\"*\"]', '2023-07-30 11:03:41', '2023-07-30 09:27:07', '2023-07-30 11:03:41'),
(17, 'App\\Models\\model_login\\model_login', 19, 'auth_token', 'c42c403bc375d273d2adc308c44b83c862d034b67ece7b9450696e1642a898db', '[\"*\"]', '2023-07-30 16:38:21', '2023-07-30 11:04:31', '2023-07-30 16:38:21'),
(18, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '884229783353dccdc8ec633fc62b5acca4563e53d32e61bdca25c09b0cd86fb3', '[\"*\"]', '2023-07-30 16:39:44', '2023-07-30 16:39:40', '2023-07-30 16:39:44'),
(19, 'App\\Models\\model_login\\model_login', 20, 'auth_token', 'decd0deb51a5b1accee202eceb2cac41a0f1c18e12c5d83efa268799d0727d7b', '[\"*\"]', NULL, '2023-07-30 16:48:57', '2023-07-30 16:48:57'),
(20, 'App\\Models\\model_login\\model_login', 20, 'auth_token', 'ba6318e910c1ae65141d9f2a6791305aa302f1a6551554c9c2acf924d44384fc', '[\"*\"]', '2023-07-30 16:51:32', '2023-07-30 16:51:30', '2023-07-30 16:51:32'),
(21, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '070fbb1a994e911f81a21b2ccde3ce24891e8c6589200ce516aeb258954c8ffc', '[\"*\"]', '2023-07-30 16:53:07', '2023-07-30 16:53:05', '2023-07-30 16:53:07'),
(22, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '670cfe1d49f60f46ec6acc04d4ca66ede7ecf5768f950f6809f2cee0327ceb04', '[\"*\"]', '2023-07-30 17:04:17', '2023-07-30 16:56:00', '2023-07-30 17:04:17'),
(23, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '9aad0d36229f09f75484fae0d989ed8cd22e7a7e1e7a281b23164ea3a02a75b3', '[\"*\"]', '2023-07-30 17:05:43', '2023-07-30 17:05:41', '2023-07-30 17:05:43'),
(24, 'App\\Models\\model_login\\model_login', 20, 'auth_token', 'b8532d06bf40c5c9a846251f4966c09ffd114c416221f8174ab5ccec5dbc8285', '[\"*\"]', '2023-07-30 17:07:49', '2023-07-30 17:07:49', '2023-07-30 17:07:49'),
(25, 'App\\Models\\model_login\\model_login', 20, 'auth_token', 'e8a4efb53306b218db056aa3e085856ab8c8bbc0b194590fbe0cfbed38590b3e', '[\"*\"]', '2023-07-30 17:09:40', '2023-07-30 17:09:08', '2023-07-30 17:09:40'),
(26, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '8f504a0be002dd818eb5ef27f13b4e407d34c1a30e7ff723795d13f19ddfac69', '[\"*\"]', '2023-07-30 17:10:13', '2023-07-30 17:10:11', '2023-07-30 17:10:13'),
(27, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '91c2d7fbcecc6d391785329a69120fed687023a364318441245a004cab7fda2a', '[\"*\"]', '2023-07-30 17:16:24', '2023-07-30 17:13:19', '2023-07-30 17:16:24'),
(28, 'App\\Models\\model_login\\model_login', 20, 'auth_token', 'eb2e8eb241dc64c6686790aca679681e3ff227066679d970d7aa84e725df5270', '[\"*\"]', '2023-07-30 17:22:37', '2023-07-30 17:16:39', '2023-07-30 17:22:37'),
(29, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '5308595f1d4ec901c9ff44b7f24c2c46d843c015db3ffc2aceb0fad86d5a5e24', '[\"*\"]', '2023-07-30 17:40:11', '2023-07-30 17:38:44', '2023-07-30 17:40:11'),
(30, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '9d1539b82c39ed3a48e327db255d3059120ca2c54ef1ba7ab7b2f34c84f40a78', '[\"*\"]', '2023-07-31 04:46:34', '2023-07-31 04:46:22', '2023-07-31 04:46:34'),
(31, 'App\\Models\\model_login\\model_login', 20, 'auth_token', '98184619197216bd58ff3631e9c061f0d0171171c86dc3ebb6cb7dec0835e1e3', '[\"*\"]', '2023-07-31 07:27:12', '2023-07-31 07:20:27', '2023-07-31 07:27:12'),
(32, 'App\\Models\\model_login\\model_login', 67, 'auth_token', 'bced8e9863917d3a21353a433ea793f0b484ae671817ba2bd16159245b50432c', '[\"*\"]', NULL, '2023-07-31 08:17:28', '2023-07-31 08:17:28'),
(33, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '68bfa05332656988edc470bc062f24f46a8a14497cfdec43e213f0ee588ead70', '[\"*\"]', '2023-07-31 09:24:05', '2023-07-31 08:20:07', '2023-07-31 09:24:05'),
(34, 'App\\Models\\model_login\\model_login', 69, 'auth_token', '640935c6cc5f6d30ed293bbd93a2dc7279bac82ca200e4c248ec7e3fb2231077', '[\"*\"]', NULL, '2023-07-31 08:33:02', '2023-07-31 08:33:02'),
(35, 'App\\Models\\model_login\\model_login', 70, 'auth_token', 'ff1d6e4f4ed1151d46b5a8e64b3886eb9853c930c66dc2afa2037b9e7356b9e9', '[\"*\"]', '2023-08-01 07:34:10', '2023-07-31 08:56:57', '2023-08-01 07:34:10'),
(36, 'App\\Models\\model_login\\model_login', 71, 'auth_token', 'b4d8a2d210272622e13604fd4262c6e059fd2157b8669fc1fa250cd763e8b11e', '[\"*\"]', NULL, '2023-08-01 08:07:32', '2023-08-01 08:07:32'),
(37, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'bcd42d27b8b7959088e489d2df2c7f6136d84d073c51a78fd58d3baae7c93b40', '[\"*\"]', '2023-08-22 12:02:37', '2023-08-06 18:52:42', '2023-08-22 12:02:37'),
(38, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'c255cc4e5460f74f45d20038afed65cdd3a74da32df7a2e280727e57af3fe2a0', '[\"*\"]', '2023-08-06 19:20:07', '2023-08-06 19:19:40', '2023-08-06 19:20:07'),
(39, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '18215c6b3324d60886a8acb0d09f9ee7ad11697f64b52d6327062adaf2c04986', '[\"*\"]', '2023-08-07 06:32:35', '2023-08-07 06:32:34', '2023-08-07 06:32:35'),
(40, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'dce834518f00bad702e9186bbca47d54a3fe1358263bec798a7f49acd3fc6c6a', '[\"*\"]', '2023-08-07 10:02:39', '2023-08-07 09:59:14', '2023-08-07 10:02:39'),
(41, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'da7c5c3f0cbd69388e795e69dc31472e4cf627f75f059ac712f79d1ae2d16af7', '[\"*\"]', '2023-08-07 10:02:39', '2023-08-07 10:01:41', '2023-08-07 10:02:39'),
(42, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'b15d4fbf1f7c8e4025927e1df977949f7862424f4a98e0bc485fe2e82459b00b', '[\"*\"]', '2023-08-07 10:03:40', '2023-08-07 10:03:39', '2023-08-07 10:03:40'),
(43, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '950947dc2aa1208a90eb112a1425ea1be223b0422e2e6f61f5c50bed930a510f', '[\"*\"]', '2023-08-07 10:05:09', '2023-08-07 10:04:20', '2023-08-07 10:05:09'),
(44, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '73801884fe7017d005a510d4324fcd2a5138d8fb52ee65ee53fbec269afe5dec', '[\"*\"]', '2023-08-07 23:36:25', '2023-08-07 23:17:45', '2023-08-07 23:36:25'),
(45, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'a8db45485193df021570cad8394fba4dc070c712fa189b123ed95cf5075a2776', '[\"*\"]', '2023-08-08 16:37:36', '2023-08-08 16:37:32', '2023-08-08 16:37:36'),
(46, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'f4a1fc90cc3aac5155b536463e0a8796bc90a49e074ad2c6a4862c013966b984', '[\"*\"]', '2023-08-12 07:38:35', '2023-08-08 16:47:02', '2023-08-12 07:38:35'),
(47, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '40183c8787fd5a7d3b0370f42485652e17117d0787d9851d82648287e3d7b02f', '[\"*\"]', '2023-08-10 00:06:26', '2023-08-10 00:06:20', '2023-08-10 00:06:26'),
(48, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '461a645a4d5713f85f44d3a81c8b131dff962ebef36808fa06623bf807d09226', '[\"*\"]', '2023-08-11 05:23:12', '2023-08-11 05:22:16', '2023-08-11 05:23:12'),
(49, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '84c81172187865b6948c7824b68ea5bc712839dfb445829ad5be0fe5e72a080f', '[\"*\"]', '2023-08-12 04:03:34', '2023-08-12 04:01:41', '2023-08-12 04:03:34'),
(50, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '640af72dc125889e6901c6f1509f57ac5752022e18507aaf9f7c4025a96547a6', '[\"*\"]', '2023-08-16 11:52:16', '2023-08-12 07:38:55', '2023-08-16 11:52:16'),
(51, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'fdcea1e379d555a294bd0b869b7153cfc6b7c6cc8a94386fcbb5d63429b4becf', '[\"*\"]', '2023-08-20 06:47:46', '2023-08-12 14:53:10', '2023-08-20 06:47:46'),
(52, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'dc3db3f1997222ec01aeab52626487bb399ebc7acf4d305f49de7870c3e159ff', '[\"*\"]', '2023-08-13 00:36:58', '2023-08-13 00:33:34', '2023-08-13 00:36:58'),
(53, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '3efe59c3ab063e183d13ec3efeba96355afad6baebf534fd9d5af597ec4337a0', '[\"*\"]', '2023-08-31 16:40:30', '2023-08-21 03:08:06', '2023-08-31 16:40:30'),
(54, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'e4c3156edfbf9891c56df67e5b29406abfdff7af8e92a00849b216a52e0581a6', '[\"*\"]', '2023-08-21 05:27:36', '2023-08-21 05:27:31', '2023-08-21 05:27:36'),
(55, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '3d42e4e444ed6d331052dd037b10d38c41e1ff8d5a7258cffdcc4084f71f53ba', '[\"*\"]', '2023-08-21 12:30:57', '2023-08-21 12:29:12', '2023-08-21 12:30:57'),
(56, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '0efccf88584ad74c1f59ff4d5e4c76cf509061995172774965141c976a1b3482', '[\"*\"]', '2023-08-23 17:24:55', '2023-08-22 05:39:36', '2023-08-23 17:24:55'),
(57, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '271212ccc3af007bffc4f4610cbf9f732c75d1f9447345283ee40aac6980ee42', '[\"*\"]', '2023-08-22 06:09:33', '2023-08-22 06:09:31', '2023-08-22 06:09:33'),
(58, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'aae3dae32f89ac046d48a06c1aaefd4708afe7bac6e2d1a6b3dc9b73fb5001be', '[\"*\"]', '2023-08-23 15:09:44', '2023-08-23 02:28:03', '2023-08-23 15:09:44'),
(59, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '860b6d23d402c3a3cebf017a748cc8ab591f3483e101f43af08b63148609d231', '[\"*\"]', '2023-08-23 15:22:18', '2023-08-23 15:10:07', '2023-08-23 15:22:18'),
(60, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'd7fb87d099f71ce0d1d43e76e4e2c8b08b2d00a6b2ddc2a505d621f700611129', '[\"*\"]', '2023-08-23 17:22:33', '2023-08-23 17:21:18', '2023-08-23 17:22:33'),
(61, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '77791ce739a23bdcbfe3f57e94fafc54384f9f93aa268804a4e9b9633e37c60b', '[\"*\"]', '2023-08-23 17:43:10', '2023-08-23 17:22:50', '2023-08-23 17:43:10'),
(62, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '401f6bc3c12846637aef2089589786742fdf44cd23708002bbef2e6591ae16d3', '[\"*\"]', '2023-08-23 17:25:21', '2023-08-23 17:25:16', '2023-08-23 17:25:21'),
(63, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '3138ebd8f408bcc0ae45b8f6f7e0fc2946de4b08005bfb39cb8c2f77d8dbe4da', '[\"*\"]', '2023-08-23 20:18:29', '2023-08-23 20:18:09', '2023-08-23 20:18:29'),
(64, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '6aa8d019c6b5fc9f37a59d360975a1ff7dbf2ac8bf5ffe9320ad52dd9eb3bebb', '[\"*\"]', '2023-09-13 12:26:21', '2023-08-23 20:19:20', '2023-09-13 12:26:21'),
(65, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'd5b639d4e2a0477e9f15525704223a2aff974abfc4108bbd31c95c0cf1fe46c3', '[\"*\"]', '2023-09-05 06:23:32', '2023-08-24 15:23:16', '2023-09-05 06:23:32'),
(66, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '951ab9b61e03ec701c5786e6f9bac092040a1d04a553253f186dfb7139f2e184', '[\"*\"]', '2023-08-26 19:29:55', '2023-08-26 19:28:59', '2023-08-26 19:29:55'),
(67, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '8aa273521b24bfa7840c5f4fe04c9821ddb4804a2ca30e845e7877255b3b7f6d', '[\"*\"]', '2023-09-05 07:17:43', '2023-08-29 17:54:08', '2023-09-05 07:17:43'),
(68, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'bf3769036ddfe8dd32cf09258b02fe6cd427848a33a75df29b722b1266d99879', '[\"*\"]', '2023-08-31 14:23:48', '2023-08-31 14:20:34', '2023-08-31 14:23:48'),
(69, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'fac791eb7197db2dbda567dc600d4364299ef8f330a946297af5f639e4527f0a', '[\"*\"]', '2023-08-31 14:30:51', '2023-08-31 14:30:51', '2023-08-31 14:30:51'),
(70, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '43f9fb8f2ee84683ab7c852856c440dbef898865a016a70a69a0a51841c3e1ad', '[\"*\"]', '2023-09-05 06:25:26', '2023-09-05 06:25:25', '2023-09-05 06:25:26'),
(71, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '2aa9a58282dff8f48f41bdd935692ab4f60f33ee9be00899083e32d56224d92c', '[\"*\"]', '2023-09-05 06:32:29', '2023-09-05 06:25:53', '2023-09-05 06:32:29'),
(72, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '0e331aa099c92afa19eb4abc1ad1d9e46288e75284aac7c197a8c7efa955deb1', '[\"*\"]', '2023-09-05 08:29:34', '2023-09-05 06:29:23', '2023-09-05 08:29:34'),
(73, 'App\\Models\\model_login\\model_login', 73, 'auth_token', '197becc615de5def1a94d3796018900b27bd7cd0d954124abb418cb5f61b9fcb', '[\"*\"]', '2023-09-05 07:41:20', '2023-09-05 07:28:13', '2023-09-05 07:41:20'),
(74, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'e662016ed0d24565df375275459c72fda3b115c083abfcb6d6f8071a0c7c70de', '[\"*\"]', '2023-09-05 07:41:55', '2023-09-05 07:41:54', '2023-09-05 07:41:55'),
(75, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '9b43c7b2e8973e979f5ec524afe1b1e8b72762d57d38866ac97a470a71a11953', '[\"*\"]', NULL, '2023-09-05 07:43:03', '2023-09-05 07:43:03'),
(76, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '65bed22de0c9ed513d40308fee8f29d537f95ea2e5e92b1c6ceda50818b5e30c', '[\"*\"]', '2023-09-05 07:43:19', '2023-09-05 07:43:04', '2023-09-05 07:43:19'),
(77, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '439872188ba6bf02d3f6fe217d5b78a04cb7e5a43cfeebac04d40017508ff382', '[\"*\"]', '2023-09-05 07:43:51', '2023-09-05 07:43:27', '2023-09-05 07:43:51'),
(78, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'a152003f7debda959928bd2e1001066735d09a792563ac2ab63c5f36707e279b', '[\"*\"]', NULL, '2023-09-05 07:44:07', '2023-09-05 07:44:07'),
(79, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'ae95a9dea63783c75dee43f98e5866412db3069f91a81b2c7e4b780a984a7086', '[\"*\"]', NULL, '2023-09-05 07:44:37', '2023-09-05 07:44:37'),
(80, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '3894520416048e1f515c3428d694ed0f0196f9955375c3273e1abfaedc061ec6', '[\"*\"]', NULL, '2023-09-05 07:45:00', '2023-09-05 07:45:00'),
(81, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'fbf427b7bf23bbf9c2725862a65c78e5a2aade822987656b4c2d5419611cd330', '[\"*\"]', NULL, '2023-09-05 07:45:26', '2023-09-05 07:45:26'),
(82, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'a63dcaf6b86c9c2e89c74a185ed4af797ab5a7704adfa7d94637c0262a23eb59', '[\"*\"]', NULL, '2023-09-05 07:45:50', '2023-09-05 07:45:50'),
(83, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '299536cb3633b07219cff9eca7ead1d1c118f4a944c8399c330e8d4e5ddb15ec', '[\"*\"]', NULL, '2023-09-05 07:46:14', '2023-09-05 07:46:14'),
(84, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '1e79af08a39d4d2c3129fbb2cd5e49afde20c5e15e83a7a5723b5394859473e9', '[\"*\"]', '2023-09-05 07:46:32', '2023-09-05 07:46:16', '2023-09-05 07:46:32'),
(85, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '5a4157e32ad744d5fcb2500890cdb8db48118b63819b0bccbb6dbc33d02eca6a', '[\"*\"]', NULL, '2023-09-05 07:46:39', '2023-09-05 07:46:39'),
(86, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '614357f85ad19cbe988529adba70a10f460b57fed4b833a3f9084c724622c9fa', '[\"*\"]', '2023-09-05 07:51:14', '2023-09-05 07:46:43', '2023-09-05 07:51:14'),
(87, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '5ddf139316439f3b133c442251e189018d71f51c2a4e62c576be94a07c57bc6a', '[\"*\"]', '2023-09-05 07:51:26', '2023-09-05 07:51:26', '2023-09-05 07:51:26'),
(88, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '0bb3faa64e67ac60e65716b9c5756f582938bff8bba519b63bb7371619a2c28b', '[\"*\"]', '2023-09-05 07:51:30', '2023-09-05 07:51:29', '2023-09-05 07:51:30'),
(89, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '4175890a71bb59f8f7e34c1975404ff69ab0bf7af83aa145243b7cf8ffdda3ca', '[\"*\"]', '2023-09-05 07:53:45', '2023-09-05 07:51:34', '2023-09-05 07:53:45'),
(90, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'c98cc0ab692cca87a37598450fe0f2b73c9eddd0738f166067343eeb979e4235', '[\"*\"]', '2023-09-05 07:54:35', '2023-09-05 07:53:55', '2023-09-05 07:54:35'),
(91, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'f2fbfe14042f6e27e27ce11a98c2634bd3227a93185db6358acbf76e1e476196', '[\"*\"]', '2023-09-05 07:56:10', '2023-09-05 07:54:46', '2023-09-05 07:56:10'),
(92, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '9c4361668480cfef2f15f5510658863bcef03bf1b9095e6c719535a7eecc0686', '[\"*\"]', '2023-09-05 08:00:26', '2023-09-05 08:00:09', '2023-09-05 08:00:26'),
(93, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'c5b8ac01fe5ceb6dd575fc00e08116df19bd61e9a349a53371a79ccaf7943724', '[\"*\"]', '2023-09-05 08:02:46', '2023-09-05 08:01:08', '2023-09-05 08:02:46'),
(94, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '2674130f38c008f41ab52a48e1d406ee70c5cb6583a4d633c33337310660cbba', '[\"*\"]', '2023-09-05 08:02:58', '2023-09-05 08:02:55', '2023-09-05 08:02:58'),
(95, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'afeae33a03fbcf9f994447e24d0bf3a4ea0ef1971865781a55ee9b955149dcb9', '[\"*\"]', '2023-09-05 08:05:50', '2023-09-05 08:03:13', '2023-09-05 08:05:50'),
(96, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '9d26300d4027f7c1297fad45d551a64812899fb40e461e659b340153191fb6d3', '[\"*\"]', '2023-09-05 08:10:09', '2023-09-05 08:06:16', '2023-09-05 08:10:09'),
(97, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '57ed7eacace468fc77e2f1c7c58b948c1c0c15bc7c103543f2b3e1e8c369e26f', '[\"*\"]', '2023-09-05 08:22:11', '2023-09-05 08:10:21', '2023-09-05 08:22:11'),
(98, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '1742b7c95c5e44f0b3384681edd97ad4ffcf61ade2f77b04f8e95d7c605964ad', '[\"*\"]', '2023-09-12 10:23:40', '2023-09-05 08:28:32', '2023-09-12 10:23:40'),
(99, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'c13269789fd6bf16384f53796faf076b24a720661e5de7c4a0d21a3ab405599c', '[\"*\"]', '2023-09-05 08:30:45', '2023-09-05 08:30:02', '2023-09-05 08:30:45'),
(100, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '1a4da860dccbe0ae72a1a8face6d83eb34c84e9a83ff059a22c95a860085b060', '[\"*\"]', '2023-09-05 08:34:33', '2023-09-05 08:33:44', '2023-09-05 08:34:33'),
(101, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'ad5d8ac8bad85139e5675c18b69aa65bca21b2f0cc573b35a456f04e0239e8b4', '[\"*\"]', '2023-09-12 10:14:54', '2023-09-05 08:39:42', '2023-09-12 10:14:54'),
(102, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'd12bb4195b24d3b6a22c056644f64f2751cfcbc4f3d22685a609d0476e0a120b', '[\"*\"]', '2023-09-05 14:55:39', '2023-09-05 14:54:44', '2023-09-05 14:55:39'),
(103, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '5f09bbed2e2e6e4c1d0372c060c912b9cc26463edb6656ca0b8560b3e08a6830', '[\"*\"]', '2023-09-05 17:50:22', '2023-09-05 17:49:57', '2023-09-05 17:50:22'),
(104, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'd1e39c10d2121614b4ca3076ea9d738109a3976a4c35a941427c185238c223c8', '[\"*\"]', '2023-09-05 17:52:25', '2023-09-05 17:52:24', '2023-09-05 17:52:25'),
(105, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'bef7a4e4f477de4af7ce9630730dbbf8e45a1492722408f27b56fd9eb1070df6', '[\"*\"]', '2023-09-07 08:36:07', '2023-09-06 03:24:55', '2023-09-07 08:36:07'),
(106, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '4519fc6cdc9a972a5483268f5a5ccf2f41d695f43f8cf91f8eb61a7f227a1e76', '[\"*\"]', '2023-09-12 09:00:15', '2023-09-07 04:32:07', '2023-09-12 09:00:15'),
(107, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '91381d0cc4bafac5793b56054624c0c00491d2a38be2c3c9164a60a96b040f21', '[\"*\"]', '2023-09-11 04:07:41', '2023-09-07 08:36:30', '2023-09-11 04:07:41'),
(108, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'e1922fcdd09b45ebf9a7703ec11d856415c3cb1a9e2dcd58ba2282ba85f575f7', '[\"*\"]', '2023-09-07 08:58:48', '2023-09-07 08:55:59', '2023-09-07 08:58:48'),
(109, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'f3fed304712eafa281a51bef81fba416d55f45f26fb7181e47f1eba058336f2d', '[\"*\"]', '2023-09-07 15:40:59', '2023-09-07 15:40:57', '2023-09-07 15:40:59'),
(110, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '6bb5a770ea906fc0a9b3a0465b1164e117c43422cc915441ecb4b3a90901c6fc', '[\"*\"]', '2023-09-07 15:41:12', '2023-09-07 15:41:11', '2023-09-07 15:41:12'),
(111, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'ea88b5ae6cddefcf8234489a637cbf029f28aa3da34a131e17da8ca732d2b26c', '[\"*\"]', '2023-09-08 23:35:51', '2023-09-08 23:35:40', '2023-09-08 23:35:51'),
(112, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '32bed77f18e3f8386cf7b5e4847791e1254d401ae01c43cf1f3fc8237f049e5d', '[\"*\"]', '2023-09-11 04:08:06', '2023-09-11 04:07:59', '2023-09-11 04:08:06'),
(113, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '0ebe1637b94c7d718a47845cc18f219ed35e83b09fb9edf7547773e898a454ac', '[\"*\"]', '2023-09-12 09:00:41', '2023-09-12 09:00:40', '2023-09-12 09:00:41'),
(114, 'App\\Models\\model_login\\model_login', 68, 'auth_token', '7cede594370debdb3d92d984043d5b65ea19250b37db7c426ac46a23c7db266a', '[\"*\"]', '2023-09-12 10:24:53', '2023-09-12 10:24:07', '2023-09-12 10:24:53'),
(115, 'App\\Models\\model_login\\model_login', 68, 'auth_token', 'e8d3e04651b26a36cfb53d6d3b901757ab5be7bcec3b187fc7504086915b23f8', '[\"*\"]', '2023-09-12 10:41:25', '2023-09-12 10:35:19', '2023-09-12 10:41:25'),
(116, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'f2a065cde21d1e0cf95a9e5b68bc290adeec3a3844f92ee673cabea93cd96618', '[\"*\"]', '2023-09-13 12:26:48', '2023-09-13 12:26:47', '2023-09-13 12:26:48'),
(117, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '822ac5725fba2dd03d0494ebb0a4a057c4de362bd3ea784b3b0690c72d19520b', '[\"*\"]', '2023-09-13 12:32:06', '2023-09-13 12:27:13', '2023-09-13 12:32:06'),
(118, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '024b6d40f37034eab3d216f76523e7fed665b0d6a38d9d498162eeba338b14f7', '[\"*\"]', '2023-09-13 12:33:18', '2023-09-13 12:32:17', '2023-09-13 12:33:18'),
(119, 'App\\Models\\model_login\\model_login', 72, 'auth_token', '2b8da25be7724e639b9273c09dfe4084ff5ccd7d691b7bef3bcf3b2c1c81f49c', '[\"*\"]', '2023-09-14 13:24:50', '2023-09-13 12:37:19', '2023-09-14 13:24:50'),
(120, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'ee9ed13fed2eed3f863432729eb72a0c9730f8c49ea2e683e3cae693f068c06c', '[\"*\"]', '2023-09-14 13:25:20', '2023-09-14 13:25:04', '2023-09-14 13:25:20'),
(121, 'App\\Models\\model_login\\model_login', 72, 'auth_token', 'eba5d1b75771261ade78c0cc487e180a87579d3e1f11a664e6ad514f5c9b0c02', '[\"*\"]', '2023-09-14 16:58:28', '2023-09-14 16:58:27', '2023-09-14 16:58:28');

-- --------------------------------------------------------

--
-- Table structure for table `rat`
--

CREATE TABLE `rat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rat`
--

INSERT INTO `rat` (`id`, `user_id`, `date_time`, `code`, `message`) VALUES
(0, 37, '2023-07-29 21:42:31', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(4, 67, '2023-03-07 11:30:27', 'Login', 'Utilizador efecuou Login no Mangala'),
(5, 67, '2023-03-07 11:32:55', 'Login', 'Utilizador efecuou Login no Mangala'),
(7, 37, '2023-03-07 11:36:07', 'Login', 'Utilizador efecuou Login no Mangala'),
(9, NULL, '2023-03-07 22:02:52', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(11, NULL, '2023-03-07 22:05:26', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(12, 35, '2023-03-07 22:05:48', 'Login', 'Utilizador efecuou Login no Mangala'),
(13, NULL, '2023-03-07 22:06:40', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(15, 35, '2023-03-08 09:10:48', 'Login', 'Utilizador efecuou Login no Mangala'),
(16, NULL, '2023-03-08 09:11:26', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(18, NULL, '2023-03-08 09:15:52', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(20, NULL, '2023-03-08 09:46:42', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(22, NULL, '2023-03-08 10:24:27', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(23, 71, '2023-03-08 10:44:42', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(24, NULL, '2023-03-08 10:45:24', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(25, 72, '2023-03-08 10:53:19', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(26, NULL, '2023-03-08 10:56:37', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(27, NULL, '2023-03-08 10:57:10', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(28, 72, '2023-03-08 10:57:27', 'Login', 'Utilizador efecuou Login no Mangala'),
(29, NULL, '2023-03-08 11:02:34', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(30, NULL, '2023-03-08 11:03:29', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(31, 73, '2023-03-08 11:12:16', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(32, NULL, '2023-03-08 11:14:06', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(33, 73, '2023-03-08 11:14:50', 'Login', 'Utilizador efecuou Login no Mangala'),
(34, NULL, '2023-03-08 11:32:17', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(35, 74, '2023-03-08 11:33:04', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(36, NULL, '2023-03-08 11:35:15', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(37, 75, '2023-03-08 11:37:51', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(38, NULL, '2023-03-08 11:41:31', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(39, 76, '2023-03-08 11:46:25', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(40, 77, '2023-03-08 11:48:59', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(41, NULL, '2023-03-08 11:51:11', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(43, NULL, '2023-03-08 12:05:15', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(44, 79, '2023-03-08 12:09:41', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(46, NULL, '2023-03-08 12:18:38', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(47, 81, '2023-03-08 12:20:57', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(48, NULL, '2023-03-08 12:21:30', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(49, 81, '2023-03-08 12:22:05', 'Login', 'Utilizador efecuou Login no Mangala'),
(50, NULL, '2023-03-08 12:25:43', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(51, 37, '2023-03-08 12:26:22', 'Login', 'Utilizador efecuou Login no Mangala'),
(52, NULL, '2023-03-08 13:01:11', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(53, 37, '2023-03-08 13:01:47', 'Login', 'Utilizador efecuou Login no Mangala'),
(54, NULL, '2023-03-08 14:02:48', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(55, 67, '2023-03-08 14:03:22', 'Login', 'Utilizador efecuou Login no Mangala'),
(56, NULL, '2023-03-08 14:08:18', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(57, 37, '2023-03-08 14:08:39', 'Login', 'Utilizador efecuou Login no Mangala'),
(58, NULL, '2023-03-08 17:13:31', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(59, 67, '2023-03-08 17:16:51', 'Login', 'Utilizador efecuou Login no Mangala'),
(60, NULL, '2023-03-08 17:17:28', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(61, 35, '2023-03-08 17:17:40', 'Login', 'Utilizador efecuou Login no Mangala'),
(62, NULL, '2023-03-08 17:20:23', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(63, 37, '2023-03-09 07:45:00', 'Login', 'Utilizador efecuou Login no Mangala'),
(64, NULL, '2023-03-09 11:31:59', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(65, 37, '2023-03-09 11:33:01', 'Login', 'Utilizador efecuou Login no Mangala'),
(66, NULL, '2023-03-09 12:32:29', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(67, 82, '2023-03-09 23:41:42', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(68, NULL, '2023-03-09 23:56:32', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(69, 83, '2023-03-10 07:35:13', 'Cadastro', 'Um(a) Utilizador(a) fez um cadastro no Mangala'),
(70, NULL, '2023-03-10 07:48:27', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(71, 83, '2023-03-10 07:49:34', 'Login', 'Utilizador efecuou Login no Mangala'),
(72, NULL, '2023-03-10 07:49:42', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(73, 83, '2023-03-10 07:50:41', 'Login', 'Utilizador efecuou Login no Mangala'),
(74, NULL, '2023-03-10 07:50:49', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(75, 83, '2023-03-10 07:51:41', 'Login', 'Utilizador efecuou Login no Mangala'),
(76, NULL, '2023-03-10 07:51:46', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(77, NULL, '2023-03-10 07:51:51', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(78, NULL, '2023-03-10 07:56:49', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(80, NULL, '2023-03-10 08:08:20', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(81, 37, '2023-03-10 08:08:42', 'Login', 'Utilizador efecuou Login no Mangala'),
(82, NULL, '2023-03-10 11:09:35', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(83, 37, '2023-03-10 11:10:51', 'Login', 'Utilizador efecuou Login no Mangala'),
(84, NULL, '2023-03-10 11:44:56', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(85, 37, '2023-03-10 11:54:26', 'Login', 'Utilizador efecuou Login no Mangala'),
(86, NULL, '2023-03-10 11:55:49', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(87, 67, '2023-03-10 11:56:00', 'Login', 'Utilizador efecuou Login no Mangala'),
(88, NULL, '2023-03-10 12:02:40', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(89, NULL, '2023-03-10 12:03:20', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(90, 67, '2023-03-10 12:03:30', 'Login', 'Utilizador efecuou Login no Mangala'),
(91, 0, NULL, 'POST', 'Uma nova den&uacute;ncia foi submetida'),
(92, 0, NULL, 'POST', 'Uma nova den&uacute;ncia foi submetida'),
(93, 67, NULL, 'DELETE', 'Uma den&uacute;ncia foi eliminada'),
(94, 67, NULL, 'DELETE', 'Uma den&uacute;ncia foi eliminada'),
(95, 67, NULL, 'Adicionar', 'Uma nova den&uacute;ncia foi submetida'),
(96, 67, NULL, 'UPDATE', 'Uma nova den&uacute;ncia foi alterada'),
(97, 67, NULL, 'Adicionar', 'Uma nova den&uacute;ncia foi submetida'),
(98, 67, NULL, 'DELETE', 'Uma den&uacute;ncia foi eliminada'),
(99, NULL, '2023-03-10 17:20:30', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(100, 37, '2023-03-10 17:20:53', 'Login', 'Utilizador efecuou Login no Mangala'),
(101, NULL, '2023-03-10 18:27:08', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(103, 35, '2023-03-10 21:50:44', 'Login', 'Utilizador efecuou Login no Mangala'),
(104, NULL, '2023-03-10 21:53:09', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(105, 37, '2023-03-10 21:53:28', 'Login', 'Utilizador efecuou Login no Mangala'),
(106, NULL, '2023-03-10 21:56:58', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(107, 67, '2023-03-12 14:55:19', 'Login', 'Utilizador efecuou Login no Mangala'),
(108, NULL, '2023-03-12 16:16:14', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(109, 37, '2023-03-12 16:47:36', 'Login', 'Utilizador efecuou Login no Mangala'),
(110, 37, '2023-03-12 21:00:04', 'Login', 'Utilizador efecuou Login no Mangala'),
(111, NULL, '2023-03-12 23:49:29', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(112, 67, '2023-03-12 23:50:16', 'Login', 'Utilizador efecuou Login no Mangala'),
(113, NULL, NULL, 'ERRO', 'Houve falha no registo duma den&uacute;ncia'),
(114, 0, NULL, 'POST', 'Uma nova den&uacute;ncia foi submetida'),
(115, 67, NULL, 'UPDATE', 'Uma nova den&uacute;ncia foi alterada'),
(116, NULL, '2023-03-13 00:45:49', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(117, 37, '2023-03-13 00:46:08', 'Login', 'Utilizador efecuou Login no Mangala'),
(118, 37, '2023-03-13 07:05:46', 'Login', 'Utilizador efecuou Login no Mangala'),
(119, 37, '2023-03-13 08:01:18', 'Login', 'Utilizador efecuou Login no Mangala'),
(120, NULL, '2023-03-13 11:05:51', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(121, NULL, '2023-03-13 11:44:08', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(122, NULL, '2023-03-13 11:45:35', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(123, NULL, '2023-03-13 11:46:17', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(124, 37, '2023-03-13 11:46:56', 'Login', 'Utilizador efecuou Login no Mangala'),
(125, 37, '2023-03-13 11:48:31', 'Login', 'Utilizador efecuou Login no Mangala'),
(126, NULL, '2023-03-13 11:53:26', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(127, NULL, '2023-03-13 11:54:25', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(128, 35, '2023-03-13 11:54:51', 'Login', 'Utilizador efecuou Login no Mangala'),
(129, NULL, '2023-03-13 11:55:54', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(130, 37, '2023-03-13 11:57:11', 'Login', 'Utilizador efecuou Login no Mangala'),
(131, 37, '2023-03-13 12:07:27', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(132, NULL, '2023-03-13 12:07:59', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(133, NULL, '2023-03-13 12:08:08', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(134, NULL, '2023-03-13 12:08:37', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(135, NULL, '2023-03-13 12:09:02', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(136, NULL, '2023-03-13 12:09:15', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(137, NULL, '2023-03-13 12:09:21', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(138, NULL, '2023-03-13 12:09:38', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(139, NULL, '2023-03-13 12:09:52', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(141, NULL, '2023-03-13 12:10:41', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(142, NULL, '2023-03-13 12:12:00', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(143, NULL, '2023-03-13 12:12:53', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(144, NULL, '2023-03-13 12:13:20', 'Senha esquecida', 'A senha do(a) Utilizador(a) foi redefinida com sucesso.'),
(146, NULL, '2023-03-13 12:14:26', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(147, NULL, '2023-03-13 12:14:40', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(148, NULL, '2023-03-13 12:15:22', 'Senha esquecida', 'A senha do(a) Utilizador(a) foi redefinida com sucesso.'),
(149, NULL, '2023-03-13 12:15:31', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(150, NULL, '2023-03-13 12:19:57', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(151, NULL, '2023-03-13 12:20:24', 'Senha esquecida', 'O(A) utilizador(a) n&atilde;o conseguiu alterar sua senha.'),
(154, 0, NULL, 'POST', 'Uma nova den&uacute;ncia foi submetida'),
(156, 0, NULL, 'POST', 'Uma nova den&uacute;ncia foi submetida'),
(158, NULL, '2023-03-14 09:42:09', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(161, NULL, '2023-03-15 10:53:55', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(163, NULL, '2023-03-15 11:37:42', 'UPDATE', 'Um perfil actualizado pelo Super Admin'),
(164, NULL, '2023-03-15 12:18:53', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(165, NULL, '2023-03-15 12:30:52', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(166, NULL, '2023-03-15 12:31:13', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(167, NULL, '2023-03-15 12:31:27', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(168, NULL, '2023-03-15 12:31:38', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(169, NULL, '2023-03-15 12:31:56', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(170, NULL, '2023-03-15 12:32:10', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(171, NULL, '2023-03-15 12:32:29', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(172, NULL, '2023-03-15 12:32:43', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(173, NULL, '2023-03-15 12:32:53', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(174, NULL, '2023-03-15 12:33:06', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(175, NULL, '2023-03-15 12:33:21', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(176, NULL, '2023-03-15 12:34:33', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(177, NULL, '2023-03-15 12:34:58', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(178, NULL, '2023-03-15 12:35:11', 'DELETE', 'Um Utilizador foi excluido pelo Super Admin'),
(179, NULL, '2023-03-15 14:19:18', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(180, NULL, '2023-03-15 14:28:45', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(181, NULL, '2023-03-15 14:28:54', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(182, NULL, '2023-03-15 14:29:05', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(183, NULL, '2023-03-15 14:29:15', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(184, NULL, '2023-03-15 14:29:28', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(185, NULL, '2023-03-15 14:29:52', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(186, NULL, '2023-03-15 14:30:52', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(187, NULL, '2023-03-15 14:31:02', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(188, NULL, '2023-03-15 14:34:22', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(189, NULL, '2023-03-15 14:34:58', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(190, NULL, '2023-03-15 14:35:10', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(191, NULL, '2023-03-15 14:35:29', 'Senha esquecida', 'A senha do(a) Utilizador(a) foi redefinida com sucesso.'),
(192, 37, '2023-03-15 14:35:36', 'Login', 'Utilizador efecuou Login no Mangala'),
(193, NULL, '2023-03-15 14:41:09', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(194, 37, '2023-03-15 15:11:28', 'Login', 'Utilizador efecuou Login no Mangala'),
(195, NULL, '2023-03-15 15:13:09', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(196, NULL, '2023-03-15 15:13:20', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(197, NULL, '2023-03-15 15:19:03', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(198, NULL, '2023-03-15 15:20:59', 'Senha esquecida', 'Um link foi gerado para o(a) Utilizador(a) alterar sua senha'),
(199, 37, '2023-03-15 15:22:41', 'Login', 'Utilizador efecuou Login no Mangala'),
(200, 37, '2023-03-16 10:34:03', 'Login', 'Utilizador efecuou Login no Mangala'),
(201, NULL, '2023-03-16 11:47:44', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(203, NULL, '2023-03-16 11:49:11', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(204, 67, '2023-03-16 11:52:39', 'Login', 'Utilizador efecuou Login no Mangala'),
(205, NULL, '2023-03-16 14:12:12', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(206, 37, '2023-03-16 14:12:49', 'Login', 'Utilizador efecuou Login no Mangala'),
(207, NULL, '2023-03-16 14:14:47', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(208, 67, '2023-03-16 14:15:01', 'Login', 'Utilizador efecuou Login no Mangala'),
(209, 67, '2023-03-16 15:17:45', 'Login', 'Utilizador efecuou Login no Mangala'),
(210, NULL, '2023-03-16 16:36:21', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(211, 37, '2023-03-16 16:39:16', 'Login', 'Utilizador efecuou Login no Mangala'),
(212, 37, '2023-03-17 11:05:28', 'Login', 'Utilizador efecuou Login no Mangala'),
(213, NULL, '2023-03-17 11:07:44', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(214, 37, '2023-03-17 11:08:21', 'Login', 'Utilizador efecuou Login no Mangala'),
(215, 37, '2023-03-17 11:13:34', 'Login', 'Utilizador efecuou Login no Mangala'),
(216, NULL, '2023-03-17 11:27:56', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(217, NULL, '2023-03-17 11:28:14', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(218, NULL, '2023-03-17 11:28:48', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(219, NULL, '2023-03-17 11:28:48', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(220, NULL, '2023-03-17 11:28:53', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(221, NULL, '2023-03-17 11:29:52', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(222, NULL, '2023-03-17 11:30:24', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(223, 37, '2023-03-17 11:30:50', 'Login', 'Utilizador efecuou Login no Mangala'),
(225, NULL, '2023-03-18 12:36:43', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(226, NULL, '2023-03-18 12:36:51', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(227, 37, '2023-03-19 18:20:19', 'Login', 'Utilizador efecuou Login no Mangala'),
(228, 37, '2023-03-19 21:03:56', 'Login', 'Utilizador efecuou Login no Mangala'),
(229, 37, '2023-03-20 08:08:26', 'Login', 'Utilizador efecuou Login no Mangala'),
(230, 37, '2023-03-21 07:48:35', 'Login', 'Utilizador efecuou Login no Mangala'),
(231, NULL, '2023-03-21 10:20:21', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(232, NULL, '2023-03-21 10:20:41', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(233, NULL, '2023-03-21 10:21:11', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(234, 37, '2023-03-21 10:21:45', 'Login', 'Utilizador efecuou Login no Mangala'),
(235, 37, '2023-03-21 17:24:22', 'Login', 'Utilizador efecuou Login no Mangala'),
(236, 37, '2023-03-24 11:38:47', 'Login', 'Utilizador efecuou Login no Mangala'),
(237, NULL, '2023-03-24 14:47:17', 'DELETE', 'Dados dum distrito foram eliminados pelo Super Admin'),
(238, NULL, '2023-03-24 15:10:23', 'POST', 'Um distrito foi adicionado pelo Super Admin'),
(239, 37, '2023-03-25 10:32:25', 'Login', 'Utilizador efecuou Login no Mangala'),
(240, NULL, '2023-03-25 12:58:26', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(241, 37, '2023-03-25 13:01:52', 'Login', 'Utilizador efecuou Login no Mangala'),
(242, NULL, '2023-03-25 13:02:58', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(243, NULL, '2023-03-25 13:03:42', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(244, NULL, '2023-03-25 13:03:59', 'Login', 'Falha no Login dum Utilizador no Mangala'),
(245, 35, '2023-03-25 13:04:22', 'Login', 'Utilizador efecuou Login no Mangala'),
(246, NULL, '2023-03-25 13:04:40', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(247, 37, '2023-03-25 13:10:20', 'Login', 'Utilizador efecuou Login no Mangala'),
(248, NULL, '2023-03-27 08:07:31', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(249, 35, '2023-03-27 08:07:36', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(250, NULL, '2023-03-27 08:07:46', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(251, 37, '2023-03-27 08:07:52', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(252, 37, '2023-03-27 09:38:01', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(253, NULL, '2023-03-31 16:48:38', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(254, NULL, '2023-03-31 16:49:10', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(255, 37, '2023-04-09 08:03:59', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(256, 37, '2023-04-09 08:23:02', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(257, 37, '2023-04-10 11:18:43', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(258, 37, '2023-04-11 11:56:34', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(259, 37, '2023-05-08 15:02:50', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(260, 37, '2023-05-26 12:50:41', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(261, 37, '2023-06-01 14:14:12', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(262, 37, '2023-06-02 09:01:41', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(263, 37, '2023-06-14 09:41:20', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(264, 37, '2023-06-28 20:38:37', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(265, 37, '2023-07-01 15:52:22', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(266, 37, '2023-07-01 15:52:25', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(267, 37, '2023-07-06 12:38:05', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(268, 37, '2023-07-06 12:41:05', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(269, NULL, '2023-07-18 20:51:22', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(270, NULL, '2023-07-18 20:51:35', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(271, NULL, '2023-07-18 20:52:02', 'Senha esquecida', 'Utilizador(a) inseriu Email inv&aacute;lido para recuperar senha'),
(272, 37, '2023-07-19 10:05:46', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(273, 37, '2023-07-19 10:07:14', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-07-31 11:48:28', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(0, NULL, '2023-07-31 11:48:32', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(0, NULL, '2023-07-31 11:48:35', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(0, NULL, '2023-07-31 11:48:37', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(0, NULL, '2023-07-31 11:48:40', 'Login', 'Falha no Login dum(a) Utilizador(a) no Mangala'),
(0, 37, '2023-07-31 12:18:16', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-07-31 12:27:50', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(0, 37, '2023-07-31 12:27:55', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-07-31 12:28:05', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(0, 37, '2023-07-31 12:28:19', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-08-06 22:41:01', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-08-07 14:48:55', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-08-22 17:12:40', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-08-23 07:07:34', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala'),
(0, 37, '2023-08-23 07:09:04', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-08-23 19:15:18', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-08-23 19:17:54', 'DELETE', 'Um tipo de den&uacute;ncia foi excluido pelo Super Admin'),
(0, NULL, '2023-08-23 19:18:03', 'DELETE', 'Um tipo de den&uacute;ncia foi excluido pelo Super Admin'),
(0, NULL, '2023-08-23 19:18:17', 'DELETE', 'Um tipo de den&uacute;ncia foi excluido pelo Super Admin'),
(0, NULL, '2023-08-23 19:19:41', 'DELETE', 'Um tipo de den&uacute;ncia foi excluido pelo Super Admin'),
(0, NULL, '2023-08-23 19:22:15', 'POST', 'Houve falha no registo de tipo de den&uacute;ncia'),
(0, 37, '2023-08-26 09:35:30', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-09-05 10:22:03', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-09-13 16:33:46', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, 37, '2023-09-14 14:35:37', 'Login', 'Utilizador(a) efectuou Login no Mangala'),
(0, NULL, '2023-09-14 14:37:12', 'LOGOUT', 'Um(a) Utilizador(a) fez Logout no Mangala');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `sitelogo` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `sitetitle` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(512) COLLATE latin1_general_ci DEFAULT NULL,
  `copyright` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `contact` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `currency` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `symbol` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `system_email` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `address` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `address2` varchar(256) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `sitelogo`, `sitetitle`, `description`, `copyright`, `contact`, `currency`, `symbol`, `system_email`, `address`, `address2`) VALUES
(1, 'mangala.png', 'Mangala', 'Plataforma de denuncia', 'Restart', '854798324', 'MOZ', 'MZ', 'mangala@mangala.co.mz', 'Av. Samuel Khankomba...', '');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `facebook` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `twitter` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `google_plus` varchar(512) COLLATE latin1_general_ci DEFAULT NULL,
  `skype_id` varchar(256) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblanexo`
--

CREATE TABLE `tblanexo` (
  `id_anexo` bigint(20) UNSIGNED NOT NULL,
  `id_denucia` int(11) NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblanexo`
--

INSERT INTO `tblanexo` (`id_anexo`, `id_denucia`, `imagem`, `audio`, `video`, `created_at`, `updated_at`) VALUES
(1, 18, '20230731114220-rn_image_picker_lib_temp_981e62dc-75af-4a84-a92e-69fde35e2584.jpg', NULL, NULL, '2023-07-31 09:18:52', '2023-07-31 09:42:20'),
(2, 18, '20230731111852-rn_image_picker_lib_temp_a80e99b4-99c6-4cff-b283-364eb42253b4.jpg', NULL, NULL, '2023-07-31 09:18:52', '2023-07-31 09:18:52'),
(3, 18, '20230731111852-rn_image_picker_lib_temp_4b757fc4-1939-494b-a59a-495d4bf620ff.jpg', NULL, NULL, '2023-07-31 09:18:52', '2023-07-31 09:18:52'),
(4, 22, '20230812165445-rn_image_picker_lib_temp_e7b05dc2-8de5-4f28-99f3-a15f72c657d2.jpg', NULL, NULL, '2023-08-12 14:54:45', '2023-08-12 14:54:45'),
(5, 22, '20230812165445-rn_image_picker_lib_temp_40c6317a-625e-458d-afd3-6eedb47b0701.jpg', NULL, NULL, '2023-08-12 14:54:45', '2023-08-12 14:54:45'),
(6, 22, '20230812165445-rn_image_picker_lib_temp_58c92a6e-58e9-4647-93a3-ca8788aec716.jpg', NULL, NULL, '2023-08-12 14:54:45', '2023-08-12 14:54:45'),
(7, 23, '20230823171151-rn_image_picker_lib_temp_fa6c56e5-bfc1-4f79-8e5e-b0eeef8671b7.jpg', NULL, NULL, '2023-08-23 15:11:51', '2023-08-23 15:11:51'),
(8, 24, '20230824084723-rn_image_picker_lib_temp_e29357c2-4764-4684-ac5d-9e6a3b92078a.jpg', NULL, NULL, '2023-08-24 06:47:23', '2023-08-24 06:47:23'),
(9, 28, '20230903085653-rn_image_picker_lib_temp_b8986eb6-dedb-4c87-a2fe-dc754dab8d22.jpg', NULL, NULL, '2023-09-03 06:56:53', '2023-09-03 06:56:53'),
(10, 32, '20230905103043-rn_image_picker_lib_temp_1509e725-30c1-4700-bd11-fe8bb5c00f21.jpg', NULL, NULL, '2023-09-05 08:30:43', '2023-09-05 08:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `tblcategoriadenucia`
--

CREATE TABLE `tblcategoriadenucia` (
  `id_categoria` bigint(20) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblcategoriadenucia`
--

INSERT INTO `tblcategoriadenucia` (`id_categoria`, `tipo`, `created_at`, `updated_at`) VALUES
(2, 'Terceiro', '2022-07-19 09:48:14', '2022-07-19 09:48:14'),
(3, 'Proprio', '2022-07-19 09:48:56', '2022-07-19 09:48:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbldadosempresa`
--

CREATE TABLE `tbldadosempresa` (
  `id_empresa` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nuit` int(11) NOT NULL,
  `contacto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avenida` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nrcasa` int(11) NOT NULL,
  `logotipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbldadosempresa`
--

INSERT INTO `tbldadosempresa` (`id_empresa`, `nome`, `nuit`, `contacto`, `contacto2`, `email`, `cidade`, `bairro`, `avenida`, `nrcasa`, `logotipo`, `created_at`, `updated_at`) VALUES
(1, 'Restart', 1111111, '84111111', '84111111', 'empresa@empresa.co.mz', 'Maputo', '-----------', '-----------', 0, NULL, '2022-07-18 13:23:50', '2022-07-18 13:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbldenucias`
--

CREATE TABLE `tbldenucias` (
  `id_denucias` bigint(20) UNSIGNED NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `idTipodenucia` int(11) DEFAULT NULL,
  `idcatdenucia` int(11) DEFAULT NULL,
  `cod_prov` int(11) DEFAULT NULL,
  `cod_cid` int(11) DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ano_nascimento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localizacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `assunto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `removido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbldenucias`
--

INSERT INTO `tbldenucias` (`id_denucias`, `id_usuario`, `idTipodenucia`, `idcatdenucia`, `cod_prov`, `cod_cid`, `nome`, `email`, `ano_nascimento`, `localizacao`, `contacto`, `sexo`, `data`, `hora`, `assunto`, `descricao`, `imagem`, `removido`, `created_at`, `updated_at`) VALUES
(1, 39, 3, 2, 6, 65, 'teste', 'email@gmail.com', '12', 'Vggghh', 'erter', 'Masculino', NULL, NULL, 'teste', 'Olá tudo', NULL, '0', '2023-07-30 16:27:05', '2023-07-30 12:40:28'),
(2, 1, 1, 3, 1, 1, NULL, NULL, NULL, NULL, 'dfgdf', NULL, NULL, NULL, NULL, 'sddfsd', NULL, '0', '2023-07-30 21:56:23', '2023-07-30 21:56:23'),
(3, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3455', NULL, NULL, NULL, NULL, 'Cgfdbh', NULL, '0', '2023-07-30 13:12:10', '2023-07-30 13:12:10'),
(4, 39, 1, NULL, 7, 35, NULL, NULL, NULL, 'Zfff', 'Zffff', NULL, NULL, NULL, NULL, 'Cbfdv', NULL, '0', '2023-07-30 13:15:32', '2023-07-30 13:15:32'),
(5, 39, 1, 1, 6, 65, NULL, NULL, NULL, 'Dfcc', 'Xccvg', NULL, NULL, NULL, NULL, 'Vvvjh', NULL, '0', '2023-07-30 13:16:51', '2023-07-30 13:16:51'),
(6, 39, 1, 2, 7, 29, NULL, NULL, NULL, 'Sccvg', '53666', NULL, NULL, NULL, NULL, 'Xvfdv', NULL, '0', '2023-07-30 14:06:09', '2023-07-30 14:06:09'),
(7, 39, 1, 2, 5, 115, NULL, NULL, NULL, 'Dccg', '3454', NULL, NULL, NULL, NULL, 'Sccv', NULL, '0', '2023-07-30 14:10:51', '2023-07-30 14:10:51'),
(8, 39, 1, 2, 7, 39, NULL, NULL, NULL, 'Sxf', 'Dfcv', NULL, NULL, NULL, NULL, 'Zcff', NULL, '0', '2023-07-30 14:12:07', '2023-07-30 14:12:07'),
(9, 39, 1, 3, 5, 117, 'Dggffgg', 'Chbby', '85589', 'Dvvvg', '8900', 'Masculino', NULL, NULL, NULL, 'Dvgg', NULL, '0', '2023-07-30 14:20:23', '2023-07-30 14:20:23'),
(10, 40, 1, 3, 1, 4, 'Tfcv', 'Fgcc', '12', 'T-3', '12345', 'Masculino', NULL, NULL, NULL, 'Teste', NULL, '0', '2023-07-30 17:39:18', '2023-07-30 17:39:18'),
(17, 89, 1, 3, 7, 35, 'Ola', 'tembebarminda4@gmail.com', '12', 'Uvhff', '045464648', 'Masculino', NULL, NULL, NULL, 'Jguh', NULL, '0', '2023-07-31 09:17:32', '2023-07-31 09:17:32'),
(18, 89, 2, 3, 7, 35, 'Ola', 'tembebarminda4@gmail.com', '12', 'Yfy', '045464648', 'Masculino', NULL, NULL, NULL, 'Hgg', NULL, '0', '2023-07-31 09:18:52', '2023-07-31 09:18:52'),
(19, 87, 3, 2, 10, 161, 'Delcio', 'teste@teste.com', '50', 'Teste', '845202445', 'Masculino', NULL, NULL, NULL, 'Hl', NULL, '1', '2023-07-31 09:21:36', '2023-08-07 10:02:39'),
(20, 87, 2, 2, 7, 161, 'Teste', 'de', '20', 'Vghhj', '86544', 'Feminino', NULL, NULL, NULL, 'null', NULL, '1', '2023-07-31 09:23:19', '2023-08-31 14:21:46'),
(21, 87, 3, 3, 1, 31, 'Delcio', 'teste@teste.com', '50', 'Tete', '845202445', 'Masculino', NULL, NULL, NULL, 'null', NULL, '1', '2023-07-31 09:23:53', '2023-08-07 10:00:01'),
(22, 91, 1, 3, 11, 17, 'Emilio Meque', 'restart.cobu@gmail.com', '43', 'Magoanine', '849332862', 'Masculino', NULL, NULL, NULL, '...', NULL, '0', '2023-08-12 14:54:45', '2023-08-12 14:54:45'),
(23, 91, 1, 3, 11, 17, 'Emilio Meque', 'restart.cobu@gmail.com', '43', 'Magoanine C', '849332862', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-08-23 15:11:51', '2023-08-23 15:11:51'),
(24, 91, 2, 3, 11, 17, 'Emilio Meque', 'restart.cobu@gmail.com', '43', 'Magoanine', '849332862', 'Masculino', NULL, NULL, NULL, 'Aaaa', NULL, '0', '2023-08-24 06:47:23', '2023-08-24 06:47:23'),
(25, 91, 2, 2, 3, 135, 'AM', 'restart@restart.Co.mz', '40', '10', '829332862', 'Masculino', NULL, NULL, NULL, 'Aaa', NULL, '0', '2023-08-24 15:24:46', '2023-08-24 15:24:46'),
(26, 87, 3, 3, 5, 116, 'Delcio', 'teste@teste.com', '50', 'undefined', '845202445', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-08-29 19:41:25', '2023-08-29 19:41:25'),
(27, 87, 8, 2, 11, 13, 'Delcio', 'teste@tests.com', '50', 'undefined', '845202445', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-08-29 20:42:41', '2023-08-31 14:23:39'),
(28, 91, 6, 3, 10, 157, 'Emilio Meque', 'restart.cobu@gmail.com', '43', 'Bbb', '849332862', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-09-03 06:56:53', '2023-09-03 06:56:53'),
(29, 91, 2, 3, 5, 112, 'Emilio Meque', 'restart.cobu@gmail.com', '43', '5', '849332862', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-09-05 06:31:35', '2023-09-05 06:31:35'),
(30, 96, 3, 3, 5, 115, 'Teste', 'Hd@te.com', '50', 'Banja', '846454564', 'Feminino', NULL, NULL, NULL, 'Bzbsbjd', NULL, '0', '2023-09-05 07:30:52', '2023-09-05 07:30:52'),
(31, 87, 4, 3, 6, 66, 'Delcio', 'teste@teste.com', '50', 'Teste', '845202445', 'Masculino', NULL, NULL, NULL, 'Tete', NULL, '0', '2023-09-05 08:03:47', '2023-09-05 08:03:47'),
(32, 91, 3, 3, 4, 52, 'Emilio Meque', 'restart.cobu@gmail.com', '43', 'Aa', '849332862', 'Masculino', NULL, NULL, NULL, 'undefined', NULL, '0', '2023-09-05 08:30:43', '2023-09-05 08:30:43'),
(33, 91, 1, 3, 4, 50, 'Emilio Meque', 'restart.cobu@gmail.com', '43', '10', '849332862', 'Masculino', NULL, NULL, NULL, 'Meu esposo', NULL, '0', '2023-09-13 12:33:15', '2023-09-13 12:33:15');

--
-- Triggers `tbldenucias`
--
DELIMITER $$
CREATE TRIGGER `Delete_denuncia` BEFORE DELETE ON `tbldenucias` FOR EACH ROW DELETE from tbldenuncias_data where id_denuncia=old.id_denucias
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `denuncias_data` AFTER INSERT ON `tbldenucias` FOR EACH ROW insert into tbldenuncias_data(id_denuncia,data_submissao, mes) values (NEW.id_denucias,now(),MONTHNAME(data_submissao))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbldenuncias_data`
--

CREATE TABLE `tbldenuncias_data` (
  `id_denuncia` int(11) NOT NULL,
  `data_submissao` date NOT NULL,
  `mes` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldenuncias_data`
--

INSERT INTO `tbldenuncias_data` (`id_denuncia`, `data_submissao`, `mes`) VALUES
(36, '2023-01-16', 'January'),
(37, '2023-01-16', 'January'),
(43, '2023-01-16', 'January'),
(44, '2023-01-16', 'January'),
(45, '2023-01-16', 'January'),
(46, '2023-01-16', 'January'),
(59, '2023-01-17', 'January'),
(60, '2023-01-17', 'January'),
(61, '2023-01-17', 'January'),
(62, '2023-01-17', 'January'),
(66, '2023-01-17', 'January'),
(69, '2023-01-17', 'January'),
(70, '2023-01-17', 'January'),
(73, '2023-01-17', 'January'),
(75, '2023-01-17', 'January'),
(77, '2023-01-17', 'January'),
(78, '2023-01-17', 'January'),
(122, '2023-03-12', 'March'),
(129, '2023-03-13', 'March'),
(17, '2023-07-31', 'July'),
(18, '2023-07-31', 'July'),
(19, '2023-07-31', 'July'),
(20, '2023-07-31', 'July'),
(21, '2023-07-31', 'July'),
(22, '2023-08-12', 'August'),
(23, '2023-08-23', 'August'),
(24, '2023-08-24', 'August'),
(25, '2023-08-24', 'August'),
(26, '2023-08-29', 'August'),
(27, '2023-08-30', 'August'),
(28, '2023-09-03', 'September'),
(29, '2023-09-05', 'September'),
(30, '2023-09-05', 'September'),
(31, '2023-09-05', 'September'),
(32, '2023-09-05', 'September'),
(33, '2023-09-13', 'September');

-- --------------------------------------------------------

--
-- Table structure for table `tbldistrito`
--

CREATE TABLE `tbldistrito` (
  `id_distrito` int(11) NOT NULL,
  `nome_distrito` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia_id` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbldistrito`
--

INSERT INTO `tbldistrito` (`id_distrito`, `nome_distrito`, `provincia_id`, `created_at`, `updated_at`) VALUES
(1, 'Matola', 1, '2022-07-18 11:38:22', '2022-07-18 11:38:22'),
(3, 'Boane', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Magude', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Manhiça', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Marracuene', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Matutuine', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Moambe', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Namaacha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'KaMpfumo', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Nlhamankulu', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'KaMaxaquene', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'KaMavota', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'KaMubukwana', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'KaTembe', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'KaNyaka', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Quelimane', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Pebane', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Nicoadala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Namarroi', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Namacurra', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Mulevala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Morrumbala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Mopeia', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Molumbo', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Mocubela', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Mocuba', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Milange', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Maganja da Costa', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Lugela', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Luabo', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Inhassunge', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Ile', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Gurué', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Gilé', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Derre', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Chinde', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Alto Molócue', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Zumbo', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Tsangano', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Tete', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Mutarara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Moatize', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Marávia', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Marara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Magoé', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Macanga', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Dôa', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Chiuta', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Chifunde', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Changara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Cahora-Bassa', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Angónia', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Nhamatanda', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Muanza', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Maringué', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Machanga', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Gorongosa', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Dondo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Chibabava', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Cheringoma', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Chemba', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Caia', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Búzi', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Beira', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Sanga', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Nipepe', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'N\'gauma', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'Muembe', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Metarica', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Mecula', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Mecanhelas', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Mavago', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Maúa', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Marrupa', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Mandimba', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Majune', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Lichinga', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Lago', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Cuamba', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Chimbonila', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Ribaué', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Rapale', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Nampula', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Nacarôa', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Nacala Porto', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Nacala-a-Velha', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Murrupula', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Muecate', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Mossuril', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Monapo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Moma', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Mogovolas', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Mogincual', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Memba', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'Mecubúri', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Meconta', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Malema', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Liúpo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Larde', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Lalaua', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Ilha de Moçambique', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Eráti', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'Angoche', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Vanduzi', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Tambara', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'Sussundenga', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'Mossurize', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'Manica', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'Macossa', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Machaze', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Macate', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Guro', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Gondola', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'Chimoio', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'Bárue', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'Zavala', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'Vilanculos', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'Panda', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'Morrumbene', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'Maxixe', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'Massinga', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'Mabote', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'Jangamo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'Inhassoro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'Inharrime', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'Inhambane', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'Homoíne', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'Govuro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'Funhalouro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'Xai-Xai', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'Massingir', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'Massangena', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'Mapai', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'Manjacaze', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'Mabalane', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'Limpopo', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'Guijá', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'Chongoene', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'Chókwè', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'Chigubo', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'Chicualacuala', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'Chibuto', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'Bilene', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'Quissanga', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'Pemba', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'Palma', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'Nangade', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'Namuno', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'Muidumbe', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'Mueda', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'Montepuez', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'Mocímboa da Praia', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'Metuge', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'Meluco', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 'Mecúfi', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 'Macomia', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 'Ibo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 'Chiúre', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 'Balama', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 'Ancuabe', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(0, 'Marromeu', 6, '2023-03-24 15:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbldistritos`
--

CREATE TABLE `tbldistritos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome_distrito` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbldistritos`
--

INSERT INTO `tbldistritos` (`id`, `nome_distrito`, `provincia_id`, `created_at`, `updated_at`) VALUES
(1, 'Matola', 1, '2022-07-18 09:38:22', '2022-07-18 09:38:22'),
(3, 'Boane', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Magude', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Manhiça', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Marracuene', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Matutuine', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Moambe', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Namaacha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'KaMpfumo', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Nlhamankulu', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'KaMaxaquene', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'KaMavota', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'KaMubukwana', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'KaTembe', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'KaNyaka', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Quelimane', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Pebane', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Nicoadala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Namarroi', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Namacurra', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Mulevala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Morrumbala', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Mopeia', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Molumbo', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Mocubela', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Mocuba', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Milange', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Maganja da Cos', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Lugela', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Luabo', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Inhassunge', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Ile', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Gurué', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Gilé', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Derre', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Chinde', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Alto Molócue', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Zumbo', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Tsangano', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Tete', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Mutarara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Moatize', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Marávia', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Marara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Magoé', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Macanga', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Dôa', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Chiuta', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Chifunde', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Changara', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Cahora-Bassa', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Angónia', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Nhamatanda', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Muanza', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Maringué', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Machanga', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Gorongosa', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Dondo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Chibabava', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Cheringoma', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Chemba', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Caia', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Búzi', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Beira', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Sanga', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Nipepe', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'N\'gauma', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'Muembe', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Metarica', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Mecula', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Mecanhelas', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Mavago', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Maúa', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Marrupa', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Mandimba', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Majune', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Lichinga', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Lago', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Cuamba', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Chimbonila', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Ribaué', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Rapale', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Nampula', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Nacarôa', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Nacala Porto', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Nacala-a-Velha', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Murrupula', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Muecate', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Mossuril', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Monapo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Moma', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Mogovolas', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Mogincual', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Memba', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'Mecubúri', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Meconta', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Malema', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Liúpo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Larde', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Lalaua', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Ilha de Moçamb', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Eráti', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'Angoche', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Vanduzi', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Tambara', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'Sussundenga', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'Mossurize', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'Manica', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'Macossa', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Machaze', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Macate', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Guro', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Gondola', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'Chimoio', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'Bárue', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'Zavala', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'Vilanculos', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'Panda', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'Morrumbene', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'Maxixe', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'Massinga', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'Mabote', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'Jangamo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'Inhassoro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'Inharrime', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'Inhambane', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'Homoíne', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'Govuro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'Funhalouro', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'Xai-Xai', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'Massingir', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'Massangena', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'Mapai', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'Manjacaze', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'Mabalane', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'Limpopo', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'Guijá', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'Chongoene', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'Chókwè', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'Chigubo', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'Chicualacuala', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'Chibuto', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'Bilene', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'Quissanga', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'Pemba', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'Palma', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'Nangade', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'Namuno', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'Muidumbe', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'Mueda', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'Montepuez', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'Mocímboa da Pr', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'Metuge', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'Meluco', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 'Mecúfi', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 'Macomia', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 'Ibo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 'Chiúre', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 'Balama', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 'Ancuabe', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 'Marromeu', 6, '2023-03-24 14:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin`
--

CREATE TABLE `tbllogin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) DEFAULT 5,
  `status` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'Activo(a)',
  `avatar` blob DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbllogin`
--

INSERT INTO `tbllogin` (`id`, `nome`, `username`, `password`, `id_usuario`, `id_perfil`, `status`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'octavio', 'octavio@gmail.com', 'cfbfc5fedd9f898c6d3484265864cb0b', 1, 0, '', '', '2022-07-18 07:20:14', '2023-01-22 09:23:12'),
(2, 'jorge', 'jorge@gmail.com', 'sha256:1000:pmYU6av9jakaHyEGYx1VN/HQVS6p6y5j:L8TbWB2iiddHwKxVdwaUSxPXkl92B9ze', 0, 0, '', '', '2022-07-18 08:42:07', '2022-07-18 08:42:07'),
(3, 'Maria', 'Marias@gmail.com', 'sha256:1000:pmYU6av9jakaHyEGYx1VN/HQVS6p6y5j:L8TbWB2iiddHwKxVdwaUSxPXkl92B9ze', 0, 0, '', '', '2022-07-26 08:04:07', '2022-07-26 08:04:07'),
(4, 'antonio', 'mariajoaopedrocastroAn@gmail.com', 'sha256:1000:pmYU6av9jakaHyEGYx1VN/HQVS6p6y5j:L8TbWB2iiddHwKxVdwaUSxPXkl92B9ze', 0, 0, '', '', '2022-07-27 06:12:44', '2022-07-27 06:12:44'),
(5, 'dasdas', 'sad@gmab.bnog', 'sha256:1000:pmYU6av9jakaHyEGYx1VN/HQVS6p6y5j:L8TbWB2iiddHwKxVdwaUSxPXkl92B9ze', 0, 0, '', '', '2022-08-01 17:22:25', '2022-08-01 17:22:25'),
(6, 'mar', 'ma@gmail.com', 'sha256:1000:pmYU6av9jakaHyEGYx1VN/HQVS6p6y5j:L8TbWB2iiddHwKxVdwaUSxPXkl92B9ze', 0, 0, '', '', '2022-08-02 12:17:14', '2022-08-02 12:17:14'),
(7, 'Tavinho', 'octaviocossa999@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 8, 5, 'Activo(a)', '', '2023-01-22 11:39:07', '2023-03-15 12:04:24'),
(8, 'Taviu', 'octaviocossa99@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 9, 0, '', '', '2023-01-22 11:42:52', '2023-01-22 11:42:52'),
(9, 'Te', 'Naj@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 10, 0, '', '', '2023-01-22 11:46:07', '2023-01-22 11:46:07'),
(10, 'Te', 'Nzbsbjb@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 11, 0, '', '', '2023-01-22 11:49:04', '2023-01-22 11:49:04'),
(11, 'Te', 'Nzbfxsbjb@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 12, 0, '', '', '2023-01-22 11:50:06', '2023-01-22 11:50:06'),
(12, 'Er', 'Jsj@na.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 13, 0, '', '', '2023-01-22 11:50:58', '2023-01-22 11:50:58'),
(13, 'Er', 'Jsj@fna.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 14, 0, '', '', '2023-01-22 11:51:34', '2023-01-22 11:51:34'),
(14, 'Er', 'Jsfj@fna.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 15, 0, '', '', '2023-01-22 11:53:16', '2023-01-22 11:53:16'),
(15, 'E', 'JJ@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 16, 0, '', '', '2023-01-22 11:56:18', '2023-01-22 11:56:18'),
(16, 'Qjj', 'Jsjhs', '85cc77cf1dbe7b8fb4e0a579c3f99450', 17, 0, '', '', '2023-01-22 11:59:30', '2023-01-22 11:59:30'),
(17, 'Qjj', 'Jsjhs@gmai.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 18, 0, '', '', '2023-01-22 12:01:06', '2023-01-22 12:01:06'),
(18, 'Qjj', 'Jsjhsg@gmai.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 19, 0, '', '', '2023-01-22 12:01:18', '2023-01-22 12:01:18'),
(19, 'Qjj', 'Jsjhshdg@gmai.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 20, 0, '', '', '2023-01-22 12:02:12', '2023-01-22 12:02:12'),
(20, 'Yah', 'F2', '85cc77cf1dbe7b8fb4e0a579c3f99450', 21, 0, '', '', '2023-02-03 09:52:30', '2023-02-03 09:52:30'),
(21, 'N', 'octaviocossa93399@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 22, 0, '', '', '2023-02-03 10:17:27', '2023-02-03 10:17:27'),
(22, 'Jk', 'octaviocossa933kg99@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 23, 0, '', '', '2023-02-03 10:25:41', '2023-02-03 10:25:41'),
(23, 'Jkd', 'octaviocoss933kg99@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 24, 0, '', '', '2023-02-03 10:26:18', '2023-02-03 10:26:18'),
(24, 'Jkdb', 'octaviocoss933g99@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 25, 0, '', '', '2023-02-03 10:28:38', '2023-02-03 10:28:38'),
(25, 'Martinha', 'Ga@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 26, 5, 'Activo(a)', '', '2023-02-03 10:29:57', '2023-03-15 11:56:44'),
(26, 'Bh', 'Gr@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 27, 0, '', '', '2023-02-03 10:32:43', '2023-02-03 10:32:43'),
(27, 'K', 'U@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 28, 0, '', '', '2023-02-03 11:37:19', '2023-02-03 11:37:19'),
(28, 'Faria', 'J@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 29, 5, 'Activo(o)', '', '2023-02-03 11:42:25', '2023-03-15 10:15:19'),
(29, 'Mingo', 'Mingo@gmail.com', '85cc77cf1dbe7b8fb4e0a579c3f99450', 30, 0, '', '', '2023-02-03 17:46:27', '2023-02-03 17:46:27'),
(34, 'Fulano Joao', 'fulano@gmail.com', '2b6f4d9ecbd0f41e76e9e38e65c9ff36', 35, 1, 'Activo(a)', '', '0000-00-00 00:00:00', '2023-03-01 13:29:29'),
(36, 'Maque', 'maque@mangala.co.mz', '7ee86ea7ca89728ef5f08aa0aef7a120', 37, 2, 'Activo(a)', '', '0000-00-00 00:00:00', '2023-03-15 13:01:18'),
(45, 'Sofia Abrao', 'sofia@mangala.co.mz', 'e47efe34ff22be438f849ff4304b01d8', 63, 1, 'Activo(a)', '', '2023-02-19 23:00:00', '0000-00-00 00:00:00'),
(49, 'Sofia Abrao', 'sofiaab@mangala.co.mz', 'a29dfc89415407f754b6e0ecd1b2c225', 67, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-02-28 20:01:17', '0000-00-00 00:00:00'),
(56, 'Eduardo Quaresma', 'eduardo.quaresma@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 75, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:37:51', '2023-03-15 11:40:23'),
(57, 'Tamires Moiane', 'tamiresmoiane@gmail.com', '3e22861235227c085f4a8009bdcb182b', 76, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:46:25', '0000-00-00 00:00:00'),
(58, 'Helio Jonasse', 'heliojonas@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 77, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:48:59', '2023-03-15 11:56:03'),
(59, 'Ana Guerra', 'anaguerra@gmail.com', '075678f780801ae6c351cd4f866997be', 78, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 11:03:12', '0000-00-00 00:00:00'),
(60, 'Tino Dino', 'tinoguerra@gmail.com', '59c0c44f8290cc2cc22e24a8ae6195c7', 79, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 11:09:41', '0000-00-00 00:00:00'),
(61, 'Tino Dito', 'tinodito@gmail.com', 'e3010bd5da58ef9cb95f0850a8fa4de6', 80, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 11:14:30', '0000-00-00 00:00:00'),
(62, 'Tino Guerra', 'tinoguerra01@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 81, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 11:20:57', '2023-03-16 15:59:09'),
(63, 'Sandra Tome', 'sandra@gmail.com', 'f9d0f83837b13bc3bce0fbf2e4caac10', 82, 5, 'Activo(a)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-09 22:41:42', '0000-00-00 00:00:00'),
(64, 'Abel Ferreira', 'abelferreira@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 83, 5, 'Activo(o)', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-10 06:35:13', '2023-03-15 09:17:41'),
(65, 'Emilio Meque', 'emiliomeque2@gmail.com', '7ee86ea7ca89728ef5f08aa0aef7a120', 84, 1, '', '', '2023-03-13 11:05:11', '0000-00-00 00:00:00'),
(66, 'Emilio Meque ', 'emiliomeque@yahoo.com.br', '7ee86ea7ca89728ef5f08aa0aef7a120', 85, 1, 'Activo(a)', '', '2023-03-17 10:26:53', '0000-00-00 00:00:00'),
(67, 'delcio', 'delciopluis@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 86, 5, 'Activo(a)', NULL, '2023-07-31 08:17:28', '2023-08-06 20:44:54'),
(68, 'Delcio', 'teste', '85cc77cf1dbe7b8fb4e0a579c3f99450', 87, 5, 'Activo(a)', NULL, '2023-07-31 08:20:07', '2023-07-31 08:20:07'),
(69, 'fsd', 'fsdf', 'd65d81c629afedfc4620552b0d7b8421', 88, 5, 'Activo(a)', NULL, '2023-07-31 08:33:02', '2023-07-31 08:33:02'),
(70, 'Ola', 'Q', '85cc77cf1dbe7b8fb4e0a579c3f99450', 89, 5, 'Activo(a)', NULL, '2023-07-31 08:56:57', '2023-07-31 08:56:57'),
(71, 'jojo todinho', 'jojo@', 'd65d81c629afedfc4620552b0d7b8421', 90, 5, 'Activo(a)', NULL, '2023-08-01 08:07:32', '2023-08-01 08:07:32'),
(72, 'Emilio Meque', 'Emiliano', 'af0617059876c7c1e1a60b3f8cfe3532', 91, 5, 'Activo(a)', NULL, '2023-08-12 14:53:10', '2023-08-12 14:53:10'),
(73, 'Teste', 'umum', '745da37f020ddb928dfd524d94ef363f', 96, 5, 'Activo(a)', NULL, '2023-09-05 07:28:13', '2023-09-05 07:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `tblprovincia`
--

CREATE TABLE `tblprovincia` (
  `id_provincia` bigint(20) UNSIGNED NOT NULL,
  `nome_provincia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblprovincia`
--

INSERT INTO `tblprovincia` (`id_provincia`, `nome_provincia`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Maputo Provincia', 'isacseguiwa@oasisit.com', '2022-07-18 07:38:01', '2022-07-18 07:38:01'),
(2, 'Inhambane', 'aurelioguerra@oasisit.com', '2022-07-18 08:02:56', '2022-07-18 08:02:56'),
(3, 'Gaza', 'helenachirindza@oasisit.com', '2022-07-18 08:02:56', '2022-07-18 08:02:56'),
(4, 'Tete', 'mangala04@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Manica', 'mangala05@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Sofala', 'mangala06@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Zambézia', 'mangala07@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Niassa', 'mangala08@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Nampula', 'mangala09@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Cabo Delgado', 'mangala10@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Maputo Cidade', 'mangala11@example.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbltipodenucia`
--

CREATE TABLE `tbltipodenucia` (
  `id_tipo_denucia` bigint(20) UNSIGNED NOT NULL,
  `tipo_denucia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbltipodenucia`
--

INSERT INTO `tbltipodenucia` (`id_tipo_denucia`, `tipo_denucia`, `created_at`, `updated_at`) VALUES
(1, 'Assedio Sexual\n', '2022-07-21 12:21:13', '2022-07-21 12:21:13'),
(2, ' Agressão física\n', '2022-07-21 12:21:13', '2022-07-21 12:21:13'),
(3, ' Violência sexual\n', '2022-07-21 12:22:03', '2022-07-21 12:22:03'),
(4, ' Violência psicológica\n', '2022-07-21 12:22:03', '2022-07-21 12:22:03'),
(5, ' Relações sexuais com menores\r\n', '2022-07-21 12:22:03', '2022-07-21 12:22:03'),
(6, ' Casamento com menores\r\n', '2022-07-21 12:22:03', '2022-07-21 12:22:03'),
(7, ' Relações sexuais com filhos\r\n', '2022-07-21 12:22:03', '2022-07-21 12:22:03'),
(8, ' Cárcere privado', '2022-07-21 12:22:03', '2022-07-21 12:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `id_usuario` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cod_prov` int(11) NOT NULL,
  `cod_cid` int(11) NOT NULL,
  `localizacao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ano_nascimento` int(11) NOT NULL,
  `id_perfil` int(11) DEFAULT 5,
  `status` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'Activo(a)',
  `avatar` blob DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblusuarios`
--

INSERT INTO `tblusuarios` (`id_usuario`, `nome`, `usuario`, `cod_prov`, `cod_cid`, `localizacao`, `email`, `contacto`, `sexo`, `ano_nascimento`, `id_perfil`, `status`, `avatar`, `created_at`, `updated_at`, `timestamp`) VALUES
(1, 'Octavio', 'Cossa', 1, 1, 't-3', 'octavio@gmail.com', '844747755', 'Masculino', 23, 5, '', '', '2022-07-13 08:27:08', '2023-01-22 07:42:05', '0000-00-00 00:00:00'),
(2, 'octavio', 'cossa', 1, 1, 'Maputo', 'octavrio2@gmail.com', '8447473', 'Masculino', 20, 5, '', '', '2022-07-18 05:20:13', '2022-07-18 05:20:13', '0000-00-00 00:00:00'),
(3, 'jorge', 'cossa', 1, 1, 't-3', 'jorge@gmail.com', '867848', 'Masculino', 12, 5, '', '', '2022-07-18 06:42:06', '2022-07-18 06:42:06', '0000-00-00 00:00:00'),
(4, 'Maria', 'antonio', 1, 1, '', 'Maria@gmail.com', '844747383', 'Feminino', 24, 5, '', '', '2022-07-26 06:04:07', '2022-07-26 10:28:54', '0000-00-00 00:00:00'),
(7, 'maria', 'mar', 1, 1, '', 'ma@gmail.com', '8488734343', 'Masculino', 12, 5, '', '', '2022-08-02 10:17:13', '2022-08-02 10:17:13', '0000-00-00 00:00:00'),
(8, 'Tavinho', 'Tav', 1, 0, '', 'octaviocossa999@gmail.com', '83637388', 'Masculino', 0, 5, 'Activo', '', '2023-01-22 10:39:06', '2023-03-15 11:04:24', '0000-00-00 00:00:00'),
(10, 'Marcos', 'Te', 1, 1, 'Matola', 'Naj@gmail.com', '8737378', 'Masculino', 12, 0, '', '', '2023-01-22 10:46:07', '2023-01-22 10:46:07', '0000-00-00 00:00:00'),
(17, 'Octavio', 'Qjj', 1, 1, 'Qsbs', 'Jsjhs', '845646464', 'Masculino', 12, 0, '', '', '2023-01-22 10:59:30', '2023-01-22 10:59:30', '0000-00-00 00:00:00'),
(18, 'Octavio', 'Qjj', 1, 1, 'Qsbs', 'Jsjhs@gmai.com', '845646464', 'Masculino', 12, 0, '', '', '2023-01-22 11:01:05', '2023-01-22 11:01:05', '0000-00-00 00:00:00'),
(19, 'Octavio', 'Qjj', 1, 1, 'Qsbs', 'Jsjhsg@gmai.com', '845646464', 'Masculino', 12, 0, '', '', '2023-01-22 11:01:18', '2023-01-22 11:01:18', '0000-00-00 00:00:00'),
(20, 'Octavio', 'Qjj', 1, 1, 'Qsbs', 'Jsjhshdg@gmai.com', '845646464', 'Masculino', 12, 0, '', '', '2023-01-22 11:02:12', '2023-01-22 11:02:12', '0000-00-00 00:00:00'),
(26, 'Martinha', 'C', 1, 0, '', 'Ga@gmail.com', '854645494', 'Masculino', 0, 5, 'Activo', '', '2023-02-03 09:29:57', '2023-03-15 10:56:44', '0000-00-00 00:00:00'),
(29, 'Faria', 'X', 1, 0, '', 'J@gmail.com', '8363', 'Masculino', 0, 5, 'Activo', '', '2023-02-03 10:42:25', '2023-03-15 09:15:19', '0000-00-00 00:00:00'),
(30, 'Mingo', 'Mingo', 8, 77, 'Namicopo', 'Mingo@gmail.com', '820666666', 'Masculino', 25, 5, '', '', '2023-02-03 16:46:27', '2023-02-03 16:46:27', '0000-00-00 00:00:00'),
(31, 'Fulano Joao', '', 1, 0, '', 'fulano@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Emilio Meque', '', 11, 0, '', 'emiliomeque2@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Abilio Semo', '', 1, 0, '', 'abilio@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Airton Senna', '', 1, 0, '', 'airton@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Fulano Joao', '', 1, 0, '', 'fulano@gmail.com', '', 'Masculino', 0, 1, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '0000-00-00 00:00:00', '2023-03-01 12:29:29', '0000-00-00 00:00:00'),
(37, 'Maque', '', 1, 0, '', 'maque@mangala.co.mz', '', 'Masculino', 0, 2, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-02-12 22:00:00', '2023-03-15 12:01:18', '0000-00-00 00:00:00'),
(67, 'Sofia Abrao', '', 1, 0, '', 'sofiaab@mangala.co.mz', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-02-28 19:01:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Eduardo Quaresma', '', 1, 0, '', 'eduardo.quaresma@gmail.com', '', 'Masculino', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 09:37:51', '2023-03-15 10:40:23', '0000-00-00 00:00:00'),
(76, 'Tamires Moiane', '', 1, 0, '', 'tamiresmoiane@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 09:46:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Helio Jonasse', '', 1, 0, '', 'heliojonas@gmail.com', '', 'Masculino', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 09:48:59', '2023-03-15 10:56:03', '0000-00-00 00:00:00'),
(78, 'Ana Guerra', '', 1, 0, '', 'anaguerra@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:03:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Tino Dino', '', 1, 0, '', 'tinoguerra@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:09:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Tino Dito', '', 1, 0, '', 'tinodito@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:14:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Tino Guerra', '', 1, 0, '', 'tinoguerra01@gmail.com', '', 'Masculino', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-08 10:20:57', '2023-03-16 14:59:09', '0000-00-00 00:00:00'),
(82, 'Sandra Tome', '', 1, 0, '', 'sandra@gmail.com', '', '', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-09 21:41:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Abel Ferreira', '', 1, 0, '', 'abelferreira@gmail.com', '', 'Masculino', 0, 5, 'Activo', 0x2e2f6173736574732f696d616765732f75736572732f757365722e706e67, '2023-03-10 05:35:13', '2023-03-15 08:17:41', '0000-00-00 00:00:00'),
(84, 'Emilio Meque', 'emiliomeque2@gmail.com', 11, 15, 'Magoanine C', 'emiliomeque2@gmail.com', '829332862', 'Masculino', 40, 1, 'Activo', '', '2023-03-13 10:05:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Emilio Meque ', 'Emilio Meque', 3, 3, 'Magoanine', 'emiliomeque@yahoo.com.br', '849332862', 'Masculino', 40, 1, 'Activo', '', '2023-03-17 09:26:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'delcio', 'delcio', 2, 0, '', 'delciopluis@gmail.com', '845202445', 'Masculino', 0, 5, 'Activo(a)', NULL, NULL, '2023-08-06 20:44:54', NULL),
(87, 'Delcio', 'teste', 4, 46, 'Teste', 'teste@teste.com', '845202445', 'Masculino', 50, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(88, 'fsd', 'fsdf', 1, 1, '3442da', 's12erwdwew', 'dfs', 'fsdf', 12, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(89, 'Ola', 'Q', 3, 140, 'Tebsb', 'tembebarminda4@gmail.com', '045464648', 'Masculino', 12, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(90, 'jojo todinho', 'jojo@', 1, 1, '3442da', 'jojo@jojo.com', 'dfs', 'fsdf', 12, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(91, 'Emilio Meque', 'Emiliano', 11, 17, 'Magoanine C', 'restart.cobu@gmail.com', '849332862', 'Masculino', 43, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(92, 'Emílio Adélia Meque', 'emiliano', 11, 17, 'Magoanine C', 'restart@restart.Co.mz', '829332862', 'Masculino', 44, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(93, 'Emílio', 'emiliano', 11, 17, 'Magoanine C', 'emeque@restart.Co.mz', '829332862', 'Masculino', 44, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(94, 'EM', 'emiliano', 11, 17, 'Magoanine C', 'emeque.sics@gmail.com', '829332862', 'Masculino', 44, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(95, 'Teste', 'teste', 5, 112, 'Matola', 'tete@te.com', '846454564', 'Feminino', 50, 5, 'Activo(a)', NULL, NULL, NULL, NULL),
(96, 'Teste', 'teste1', 5, 112, 'Matola', 'Hd@te.com', '846454564', 'Feminino', 50, 5, 'Activo(a)', NULL, NULL, NULL, NULL);

--
-- Triggers `tblusuarios`
--
DELIMITER $$
CREATE TRIGGER `delete_login_user` BEFORE DELETE ON `tblusuarios` FOR EACH ROW DELETE from tbllogin where tblusuarios.id_usuario=tbllogin.id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbperfil`
--

CREATE TABLE `tbperfil` (
  `codPerfil` int(2) NOT NULL,
  `papel` varchar(30) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbperfil`
--

INSERT INTO `tbperfil` (`codPerfil`, `papel`) VALUES
(1, 'Administrador(a)'),
(2, 'Super Admin'),
(5, 'Denunciante');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `user_id`, `created_at`) VALUES
(36, '109f653fa395f6f55a4ee2e3517cd5', 67, '2023-03-04'),
(37, '15dae67c6515baf7a73f6ee028e5de', 67, '2023-03-04'),
(38, '808fa84c5b1102d42765a0b44ba08e', 67, '2023-03-04'),
(39, 'c5d0967aeb2538e495e69671ddfdb6', 67, '2023-03-04'),
(40, 'de4c122b3f762b2e7fe434a8b75b5a', 67, '2023-03-05'),
(41, 'c3d95fef418310d2dc88678ef62202', 67, '2023-03-05'),
(42, '8f2227c3c84576bd9ef8350ef2bfed', 67, '2023-03-05'),
(43, 'c71b9e227c7fb9eade387e73e2d384', 35, '2023-03-05'),
(44, '87b97fa4a9c84bb9b55c74eb0a9a5d', 67, '2023-03-05'),
(45, '61cd0a4fae48ba43b9b3f471b86473', 67, '2023-03-05'),
(46, 'bbf88907c7ca0018e28ba0d960fd81', 67, '2023-03-05'),
(47, 'ec824cf1657a75d003f819307847be', 67, '2023-03-05'),
(48, '4fffdfbc1d5e50bfa13bc647646113', 67, '2023-03-05'),
(49, '58fa7a7e1dc7a3412b60b8b15b1c85', 67, '2023-03-05'),
(50, 'bfb7923e576df844406195e007c482', 35, '2023-03-05'),
(51, '0678328a7babe68221d8f3e8bd9047', 67, '2023-03-05'),
(52, '24dbec8cdb4120202a4ac66dbdefc3', 67, '2023-03-05'),
(53, 'ec5e5293a62f4f4b24d2677c21f018', 67, '2023-03-05'),
(54, '9e359d5112b8d1bfb32e0ec38a03a9', 35, '2023-03-05'),
(55, 'f91edd6f44296660db68e6341888ee', 35, '2023-03-05'),
(56, 'c1f2b11481390b1d74d56514006efd', 67, '2023-03-06'),
(57, '4862cd30be16280d554a21730bc118', 67, '2023-03-06'),
(58, 'd3e0b29d9e446383e7b2a95308c2b4', 67, '2023-03-06'),
(59, 'e7c58b191e793a7277c55e41121577', 67, '2023-03-06'),
(60, '70993901fb53da3e6567b479665e12', 67, '2023-03-06'),
(61, 'fadcc1228045d3c99db66cbdfdfc55', 67, '2023-03-06'),
(62, '058ef6cc25ec505a4d768d5a2f1b98', 67, '2023-03-06'),
(63, '347a6496e4d88fb27e9f789401c756', 67, '2023-03-06'),
(64, '3d695835ab1b4a88d6ca568d427997', 67, '2023-03-06'),
(65, '76d31861da980dd62dd2dacfe16a64', 67, '2023-03-06'),
(66, 'ee2623278baee1c2ca0175d579956f', 67, '2023-03-06'),
(67, '26a187d0fd0aac0e82b03628da45c1', 67, '2023-03-06'),
(68, '3e72b95966bf9d895cde503eaa0f77', 67, '2023-03-06'),
(69, '93c7a042d4a637b940777c3eb73db2', 67, '2023-03-06'),
(70, '2054d1cfae31b684da4873d84ef252', 67, '2023-03-06'),
(71, 'a880dc34ecda891350442366664077', 67, '2023-03-06'),
(72, '90d630276761df6e7654e8d328d014', 67, '2023-03-06'),
(73, '775995c4c7c0c7e1f1720352f71ad9', 67, '2023-03-06'),
(74, 'eb8377281c5a69f52884252fe79fb8', 67, '2023-03-06'),
(75, '8a86dd1d88c0d1879d1ecc178caaaf', 67, '2023-03-06'),
(76, '13352ab022e1cb219932b95371750d', 67, '2023-03-06'),
(77, 'c57e79f102c69541777d99264fe8d5', 67, '2023-03-06'),
(78, 'f2b2d60cd8c06f4c388a559f7148ed', 67, '2023-03-06'),
(79, 'f98fcff41bd9d0613c6c27f9839c88', 67, '2023-03-06'),
(80, '1a999bd5b25df76f88e0b4242abc0e', 67, '2023-03-06'),
(81, 'f87bc6eafcf6b24de46f5e3cd3fea6', 67, '2023-03-06'),
(82, '7b769b385fb5b070814b4c07ebfe44', 67, '2023-03-06'),
(83, 'c3f0653b7f6b551ee938d82997d840', 67, '2023-03-06'),
(84, '7501db67f29db23e19e98bd39afdc0', 37, '2023-03-06'),
(85, '52ef53815423a695b5b3db423ddd73', 67, '2023-03-06'),
(86, '08217f0f5c30f7b9d2038cdccdaaae', 67, '2023-03-06'),
(87, 'f2cec26d90fccc4b03f4ae7dc4ab24', 37, '2023-03-06'),
(88, 'b99724bfa72a176e9311542c8435e4', 67, '2023-03-06'),
(89, '45f4e75fe50753e10c0a5584d7c812', 37, '2023-03-13'),
(90, '7444f798c5c7fea5f9b371666a7ec8', 37, '2023-03-13'),
(91, '03963d97cdf63a6e50b484124e688c', 37, '2023-03-13'),
(92, 'f228f705fcf5e78cfe29f0bf590dd6', 37, '2023-03-15'),
(93, '2d69ce110e17b922117c78716de8f8', 37, '2023-03-15'),
(94, '2260fb360e730095f79be0b55c1f26', 37, '2023-03-15'),
(95, 'cd59c7780cf2eccbda2dc4d2cb617f', 37, '2023-03-15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', 'teste@gmail.com', NULL, '12', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tblanexo`
--
ALTER TABLE `tblanexo`
  ADD PRIMARY KEY (`id_anexo`);

--
-- Indexes for table `tblcategoriadenucia`
--
ALTER TABLE `tblcategoriadenucia`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `tbldenucias`
--
ALTER TABLE `tbldenucias`
  ADD PRIMARY KEY (`id_denucias`);

--
-- Indexes for table `tbldistritos`
--
ALTER TABLE `tbldistritos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbllogin`
--
ALTER TABLE `tbllogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblprovincia`
--
ALTER TABLE `tblprovincia`
  ADD PRIMARY KEY (`id_provincia`) USING BTREE;

--
-- Indexes for table `tbltipodenucia`
--
ALTER TABLE `tbltipodenucia`
  ADD PRIMARY KEY (`id_tipo_denucia`);

--
-- Indexes for table `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `tblanexo`
--
ALTER TABLE `tblanexo`
  MODIFY `id_anexo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblcategoriadenucia`
--
ALTER TABLE `tblcategoriadenucia`
  MODIFY `id_categoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbldenucias`
--
ALTER TABLE `tbldenucias`
  MODIFY `id_denucias` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbldistritos`
--
ALTER TABLE `tbldistritos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `tbllogin`
--
ALTER TABLE `tbllogin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `tblprovincia`
--
ALTER TABLE `tblprovincia`
  MODIFY `id_provincia` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbltipodenucia`
--
ALTER TABLE `tbltipodenucia`
  MODIFY `id_tipo_denucia` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblusuarios`
--
ALTER TABLE `tblusuarios`
  MODIFY `id_usuario` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
